rowLinearPW <- function(x, cuts) {
  ni <- length(cuts)-1
  sapply(1:ni, function(i) {
    L <- cuts[i]
    U <- cuts[i+1]
    if (x < L) {
      0
    } else if (x < U) {
      x - L
    } else {
      U - L
    }
  })
}

#' A function to generate piecewise inputs
#'
#' This function builds a matrix of n-1 inputs from a given array of cuts
#' @param values the variable to cut into pieces
#' @param cuts the limits of the intervals (or pieces) to perform on the value
#' @return the matrix of the inputs
#' @export
#' @examples
#'    values <- log(1:100)
#'    cuts <- c(log(c(1,5,10)), Inf)
#'    lpw <- linearPW(values, cuts)
#'    head(lpw)
linearPW <- function(values, cuts) {
  rows <- lapply(values, function(x) rowLinearPW(x, cuts))
  mat <- do.call(rbind, rows)
  ni <- length(cuts) - 1
  colnames(mat) <- sapply(1:ni, function(i) paste0('I', i))
  return(mat)
}
